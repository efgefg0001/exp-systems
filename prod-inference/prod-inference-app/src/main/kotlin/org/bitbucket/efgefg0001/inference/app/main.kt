package org.bitbucket.efgefg0001.inference.app

fun <T> MutableList<T>.swap(index1: Int, index2: Int) {
    val tmp = this[index1]
    this[index1] = this[index2]
    this[index2] = tmp
}

fun main(args: Array<String>) {
    val lst = mutableListOf("one", "two", "three")
    lst.swap(0, 2)
    println("lst = $lst")
}
package org.bitbucket.efgefg0001.inference.lib

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Test
import java.lang.Thread.currentThread

fun loadIndirect(): IndirectInputData {
    val mapper = jacksonObjectMapper()
    val input = currentThread().contextClassLoader.getResourceAsStream("indirect.json")
    return mapper.readValue(input)
}

fun printResult(result: List<String>) {
    result.forEach {
        println(it)
    }
}

class IndirectDirectTest {
    @Test
    fun indirect_simple_ok() {
        try {
            var input = loadIndirect()
            checkInference(input)
            println("YES")
            input = loadIndirect()
            val actual = indirectInference(input)
            printResult(actual)
        } catch (ex: Exception) {
            println("NO")
        }
    }
}

fun checkInference(indInput: IndirectInputData) {
    val input = InputData(facts = indInput.facts, rules = indInput.rules, query = indInput.query.keys.toList())
    val storage =  input.facts
    val query = input.query.toMutableList()

    var i = 0;

    while(query.isNotEmpty() && i++ < DIRECT_INFERENCE_MAX_CYCLES) {
        for (rule in input.rules) {
            val activate = rule.condition.entries
                    .filter { pair -> storage[pair.key] != pair.value}
                    .toSet()
                    .isEmpty()
            if (activate)
                rule.conclusion.entries
                        .forEach { entry ->
                            storage[entry.key] = entry.value
                            query.remove(entry.key)
                        }
        }
    }

    val trololo = input.query
            .filter { storage[it] != indInput.query[it] }
            .isNotEmpty()
    if (trololo)
        throw Exception("can't infer")

}

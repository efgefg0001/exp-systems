package org.bitbucket.efgefg0001.inference.lib

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.lang.Thread.currentThread

fun loadInput(): InputData {
    val mapper = jacksonObjectMapper()
    val input = currentThread().contextClassLoader.getResourceAsStream("input.json")
    return mapper.readValue(input)
}

class DirectTest {
    @Test
    fun direct_simple_ok() {
        val input = loadInput()
        val expected = mapOf("d" to "4")

        val actual = directInference(input)
        println("Answer: ${actual}")

//        assertEquals(expected, actual)
    }
}
package org.bitbucket.efgefg0001.inference.lib

data class InputData(val rules: List<Rule>, val facts: LinkedHashMap<String, String>, val query: List<String>)

data class IndirectInputData(val rules: List<Rule>, val facts: LinkedHashMap<String, String>, val query: LinkedHashMap<String, String>)

data class Rule(val condition: LinkedHashMap<String, String>, val conclusion: LinkedHashMap<String, String>)

data class Pair<A, B>(val key: A, val value: B)

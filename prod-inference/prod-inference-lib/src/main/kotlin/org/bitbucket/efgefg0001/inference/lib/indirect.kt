package org.bitbucket.efgefg0001.inference.lib

import java.util.concurrent.atomic.AtomicInteger

const val INDIRECT_INFERENCE_MAX_CYCLES = 10

fun indirectInference(input: IndirectInputData, maxCycles: Int = INDIRECT_INFERENCE_MAX_CYCLES): List<String> {

    val query = input.query

    val depth = AtomicInteger(0)

    val inference = mutableListOf<String>()
    traverse(query, input, inference, depth)

    return inference
}

fun traverse(query: Map<String, String>, input: IndirectInputData, inference: MutableList<String>, depth: AtomicInteger) {
    depth.incrementAndGet()
    if (depth.get() > INDIRECT_INFERENCE_MAX_CYCLES)
        return

    for (goal in query) {
        for (rule in input.rules) {
            if (rule.conclusion[goal.key] == goal.value) {
                inference.add("${goal} -> ${rule.condition}")

                val newQuery = rule.condition.entries
                        .filter { !input.facts.containsKey(it.key) }
                        .associateBy ( {it.key}, {it.value} )
                if (newQuery.size != 0)
                    traverse(newQuery, input, inference, depth)
            }
        }
    }
}

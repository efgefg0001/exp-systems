package org.bitbucket.efgefg0001.inference.lib

const val DIRECT_INFERENCE_MAX_CYCLES = 10000;

fun directInference(input: InputData): Map<String, String> {
    val storage =  input.facts
    val query = input.query.toMutableList()

    var i = 0;

    while(query.isNotEmpty() && i++ < DIRECT_INFERENCE_MAX_CYCLES) {
        for (rule in input.rules) {
            val activate = rule.condition.entries
                    .filter { pair -> storage[pair.key] != pair.value}
                    .toSet()
                    .isEmpty()
            if (activate)
                rule.conclusion.entries
                        .forEach { entry ->
                            storage[entry.key] = entry.value
                            query.remove(entry.key)
                        }
        }
    }

    return input.query
            .associateBy ( {it}, {storage[it] ?: "UNKNOWN"}  )

}